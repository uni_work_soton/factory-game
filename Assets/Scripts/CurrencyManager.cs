using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CurrencyManager : MonoBehaviour {
    public static CurrencyManager Instance {get; set;}

    [SerializeField] private long money;
    

    // Start is called before the first frame update
    void Start() {
        Instance = this;
        money = 50;
    }

    public long GetMoney(){
        return this.money;
    }

    public void SetMoney(int money){
        this.money = money;
    }

    public void SubtractMoney(long cost){
        money-=cost;
    }

    public void AddMoney(float amount){
        money+= (int) amount;
    }
}
