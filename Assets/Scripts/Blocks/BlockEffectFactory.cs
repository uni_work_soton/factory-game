using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://straypixels.net/statuseffects-framework/
public abstract class BlockEffectFactory: ScriptableObject {
    public abstract BlockEffect GetBlockEffect(Block target);
}
//attach implementation of this to an upgrader and it will apply the block effect to the blocks on contact
//GetBlockEffect(CollidingBlock).Apply()

public class BlockEffectFactory<EffectData, BlockEffectType>: BlockEffectFactory where BlockEffectType: BlockEffect<EffectData>, new() {

    public EffectData data;
    public override BlockEffect GetBlockEffect(Block target) {
        return new BlockEffectType {data = this.data, target = target};
    }
}

public abstract class BlockEffect {
    public abstract void ApplyTouch(Transform applicator);
    public abstract void ApplyConstant(Transform applicator);
}

public abstract class BlockEffect<EffectData> : BlockEffect {
    public Block target;
    public EffectData data;
}
