using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Block", menuName = "ScriptableObjects/Block")]
public class BlockSO : ScriptableObject {

    public string blockName;
    public float size = 0.5f;
    public float startingValue;
    public Material material;
    public PhysicMaterial physicsMaterial;
    public List<BlockEffectFactory> startingBlockEffects;
    public List<string> startingBlockTags; //E.g. Ferrous, Conductive, tags that might get bonus effects for certain upgraders

}
