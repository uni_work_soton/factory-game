using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour {
    public static BlockManager Instance;
    private Stack<Block> blockPool;
    private List<Block> activeBlocks;
    public int poolSize = 100;
    [SerializeField] public int activeBlockCount = 0;

    private int timesCleared = 0;
    public event EventHandler OnClear;

    public AudioClip clear;

    public AudioSource audioSource;

    public bool spawningEnabled;


    private void Awake() {
        audioSource = GetComponent<AudioSource>();

        Instance = this;
        blockPool = new Stack<Block>();
        activeBlocks = new List<Block>();
    }

    private void Start() {
        spawningEnabled = false;
        
        for(int i = 0; i<poolSize; i++) {
            Block block = Block.BlankBlock();
            block.gameObject.transform.parent = transform;
            blockPool.Push(block);

        }
        //Generate Pooled Blocks
    }

    private void Update() {
        //for testing purposes, lets the dev see the active block count in the editor
        activeBlockCount = activeBlocks.Count;
    }

    public void ClearFactory() {
        new List<Block>(activeBlocks).ForEach(block => ReturnToPool(block));
        timesCleared++;
        OnClear?.Invoke(this, EventArgs.Empty);
        audioSource.PlayOneShot(clear,2f);
    }

    public void ReturnToPool(Block block) {
        block.CleanupBlock();
        blockPool.Push(block);
        activeBlocks.Remove(block);
    }

    public void SpawnBlock (BlockSO toSpawn, Vector3 spawnPos) {
        if(spawningEnabled) {
            if (blockPool.Count > 0) {
                Block block = blockPool.Pop();
                activeBlocks.Add(block);
                block.SetupBlock(toSpawn);
                block.gameObject.transform.position = spawnPos;
            }
        }
    }

    public int GetTimesCleared() {
        return timesCleared;
    }
}