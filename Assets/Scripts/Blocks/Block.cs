using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    public BlockSO blockSO;
    public float value;
    //private float size;
    public List<string> tags;
    public Dictionary<string,int> upgradeTags;

    public float instability;

    public static Block BlankBlock() {
        GameObject blockObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        blockObj.transform.position = new Vector3(-10,10,-10);
        Block block = blockObj.AddComponent<Block>();
        blockObj.tag = "BeltItem";
        blockObj.AddComponent<Rigidbody>();
        blockObj.layer = 8;

        block.CleanupBlock();
        return block;
    }

    public void CleanupBlock() {
        transform.position = new Vector3(-10,10,-10);
        
        gameObject.SetActive(false);
        StopAllCoroutines();
        value = 0;
        tags = new List<string>();
        upgradeTags = new Dictionary<string,int>();
        blockSO = null;
        instability = 0;

        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0); 

        //not needed yet unless we implent features that modify it.
        //set object mesh back to cube if not a cube
        //removing particle systems back to particle pool
    }

    public void SetupBlock(BlockSO toSpawn) {
        blockSO = toSpawn;
        value = toSpawn.startingValue;
        tags = toSpawn.startingBlockTags;
        upgradeTags = new Dictionary<string,int>();

        GetComponent<Renderer>().material = toSpawn.material;
        GetComponent<Collider>().material = toSpawn.physicsMaterial;

        transform.localScale = new Vector3(toSpawn.size,toSpawn.size,toSpawn.size) * 0.16f;

        toSpawn.startingBlockEffects.ForEach(factory => factory.GetBlockEffect(this).ApplyTouch(transform));


        gameObject.SetActive(true);
    }

    private void FixedUpdate() {
        Vector3 pos = transform.position;
        if( ((pos.x > 0f && pos.z > 0f) && pos.y < 0f) || (pos.y < -5.12f)) {
            BlockManager.Instance.ReturnToPool(this);
        }
    }

    public void Collect() {
        Collect(1.0f);
    }

    public void Collect(float mult) {
        CurrencyManager.Instance.AddMoney(value * mult);
        BlockManager.Instance.ReturnToPool(this);
    }

    public void Explode(float explosionRadius = 0.32f, float explosionForce = 2.5f) {
        AudioClip explosionSound = Resources.Load<AudioClip>("Sounds/explosion");
        AudioSource.PlayClipAtPoint(explosionSound, new Vector3(-10,10,-10),0.7f);
        
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
        foreach (Collider hit in colliders) {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && hit.gameObject.tag == "BeltItem") {
                rb.AddExplosionForce(explosionForce, explosionPos, explosionRadius, 0.08f, ForceMode.Impulse);
            }
        }
        Collect(0f);

    }

    public void InstabilityCheck() {
        float roll = Random.Range(0f,1f);
        if (roll < instability) {
            Explode();
        }
    }
    
}
