using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Arcane Infuser Block Effect", menuName = "ScriptableObjects/BlockEffects/ArcaneInfuser")]
public class ArcaneInfuserBlockEffectFactory : BlockEffectFactory<ArcaneInfuserBlockEEffectData, ArcaneInfuserBlockEEffect> {}

[System.Serializable]
public class ArcaneInfuserBlockEEffectData {
    public float valueMult = 2.0f;
}

public class ArcaneInfuserBlockEEffect : BlockEffect<ArcaneInfuserBlockEEffectData> {

    //has access to the target variable of its parent so it knows where to apply the script
    public override void ApplyTouch(Transform applicator) {
        target.InstabilityCheck();
        if (!(target.upgradeTags.Count > 0)) {
            target.value = target.value * data.valueMult;
            target.upgradeTags.Add("ArcaneInfuser",1);
        }
        else {
            target.Explode();
        }
    }
    public override void ApplyConstant(Transform applicator) {}

}