using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Accelerator Block Effect", menuName = "ScriptableObjects/BlockEffects/Accelerator")]
public class AcceleratorBlockEffectFactory : BlockEffectFactory<AcceleratorBlockEffectData, AcceleratorBlockEffect> {}


[System.Serializable]
public class AcceleratorBlockEffectData {
    public float acceleration = 1.0f;
}

public class AcceleratorBlockEffect : BlockEffect<AcceleratorBlockEffectData> {

    public override void ApplyTouch(Transform applicator) {}
    //has access to the target variable of its parent so it knows where to apply the script
    public override void ApplyConstant(Transform applicator) {
        target.gameObject.GetComponent<Rigidbody>().velocity = applicator.forward * data.acceleration;
        //attach the process function to the block class and call it with this function, maybe take a block -> void action as a parameter for bonus processing effects?
    }
}
