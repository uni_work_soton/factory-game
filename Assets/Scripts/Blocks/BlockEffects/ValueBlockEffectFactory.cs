using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Value Block Effect", menuName = "ScriptableObjects/BlockEffects/Value")]
public class ValueBlockEffectFactory : BlockEffectFactory<ValueBlockEffectData, ValueBlockEffect> {}

[System.Serializable]
public class ValueBlockEffectData {
    public float valueAdd = 0.0f;
    public float valueMult = 1.0f;
    public float valueMax = 100000.0f;
}

public class ValueBlockEffect : BlockEffect<ValueBlockEffectData> {

    //has access to the target variable of its parent so it knows where to apply the script
    public override void ApplyTouch(Transform applicator) {
        target.value += data.valueAdd;
        target.value = target.value * data.valueMult;
    }
    public override void ApplyConstant(Transform applicator) {}

}