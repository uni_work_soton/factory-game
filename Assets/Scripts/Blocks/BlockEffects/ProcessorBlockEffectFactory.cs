using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Processor Block Effect", menuName = "ScriptableObjects/BlockEffects/Processor")]
public class ProcessorBlockEffectFactory : BlockEffectFactory<ProcessorBlockEffectData, ProcessorBlockEffect> {}

[System.Serializable]
public class ProcessorBlockEffectData {
    public float valueMult = 1.0f;
}

public class ProcessorBlockEffect : BlockEffect<ProcessorBlockEffectData> {

    //has access to the target variable of its parent so it knows where to apply the script
    public override void ApplyTouch(Transform applicator) {
        target.Collect(data.valueMult);
        //attach the process function to the block class and call it with this function, maybe take a block -> void action as a parameter for bonus processing effects?

        //sound for destroying
        AudioClip destroying = Resources.Load<AudioClip>("Sounds/blockCollect");
        AudioSource.PlayClipAtPoint(destroying, new Vector3(-10,10,-10),0.7f);
    }
    public override void ApplyConstant(Transform applicator) {}
}