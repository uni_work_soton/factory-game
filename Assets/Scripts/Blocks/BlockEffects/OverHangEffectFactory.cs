using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newOverHangBlockEffect", menuName = "ScriptableObjects/BlockEffects/OverHang")]
public class OverHangEffectFactory : BlockEffectFactory<OverHangBlockEffectData, OverHangBlockEffect> {}


[System.Serializable]
public class OverHangBlockEffectData {
    public float valueMult1;
    public float valueMult2;
    public float valueMult3;
}

public class OverHangBlockEffect : BlockEffect<OverHangBlockEffectData> {

    public override void ApplyTouch(Transform applicator) {
        target.InstabilityCheck();
        if (!target.upgradeTags.ContainsKey("OverHang")) {
            target.value = target.value * data.valueMult1;
            target.upgradeTags.Add("OverHang",1);
        }
        if (target.upgradeTags["OverHang"] == 1) {
            target.value = target.value * data.valueMult2;
            target.upgradeTags["OverHang"]++;
        }
        if (target.upgradeTags["OverHang"] == 2) {
            target.value = target.value * data.valueMult3;
            target.upgradeTags["OverHang"]++;
        }
        if (target.upgradeTags["OverHang"] == 3) {
            target.Explode();
        }
    }
    public override void ApplyConstant(Transform applicator) {
    }
}