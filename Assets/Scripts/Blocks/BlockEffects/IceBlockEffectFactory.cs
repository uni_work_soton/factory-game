using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newIceBlockEffect", menuName = "ScriptableObjects/BlockEffects/Ice")]
public class IceBlockEffectFactory : BlockEffectFactory<IceBlockEffectData, IceBlockEffect> {}


[System.Serializable]
public class IceBlockEffectData {
    public PhysicMaterial material;
    public float valueMult;
}

public class IceBlockEffect : BlockEffect<IceBlockEffectData> {

    public override void ApplyTouch(Transform applicator) {
        target.InstabilityCheck();
        if (!target.upgradeTags.ContainsKey("ice")) {
            target.upgradeTags.Add("ice",1);
            target.value = target.value * data.valueMult;
            target.GetComponent<Collider>().material = data.material;
        }
    }
    public override void ApplyConstant(Transform applicator) {
    }
}