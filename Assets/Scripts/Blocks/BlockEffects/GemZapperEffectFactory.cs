using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newGemZapperBlockEffect", menuName = "ScriptableObjects/BlockEffects/GemZapper")]
public class GemZapperEffectFactory : BlockEffectFactory<GemZapperBlockEffectData, GemZapperBlockEffect> {}


[System.Serializable]
public class GemZapperBlockEffectData {
    public float instabilityIncrease;
    public int maxTimes;
    public float valueMult;
}

public class GemZapperBlockEffect : BlockEffect<GemZapperBlockEffectData> {

    public override void ApplyTouch(Transform applicator) {
        if (!target.upgradeTags.ContainsKey("GemZapper")) {
            target.upgradeTags.Add("GemZapper",0);
        }

        target.upgradeTags["GemZapper"]++;
        if (target.upgradeTags["GemZapper"] > data.maxTimes) {
            target.instability += data.instabilityIncrease;
        }
        target.InstabilityCheck();
        target.value = target.value * data.valueMult;
    }
    public override void ApplyConstant(Transform applicator) {
    }
}