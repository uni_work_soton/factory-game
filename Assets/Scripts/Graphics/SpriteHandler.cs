
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

//The purpose of this class is very simple, to get objects their sprites, and to load them in if they arent already.
public class SpriteHandler : MonoBehaviour {

    public static SpriteHandler Instance { get; private set; }

    private Dictionary<string,Texture2D[][][]> structures;

    public Material structureSpriteMaterial;

    public int upscaleAmount;


    //all sprites need to be offset this much + 2x for every tile diagonal they are back.
    private float yPositionUnit;

    private void Awake() {
        Instance = this;
        structures = new Dictionary<string,Texture2D[][][]>();
    }
    private void Start(){
        //LoadDebugSprite();
    }

    public event EventHandler AnimationUpdate;
    public int FPS;
    public int animationFrame = 0;
    
    private void Update() {
        int frame = (int)(Time.time * FPS);
        if (frame == animationFrame) return;
        animationFrame = frame % FPS; 
        AnimationUpdate?.Invoke(this, EventArgs.Empty);

        yPositionUnit = (float) (Math.Sqrt(2) / Math.Sqrt(3));
    }


    public GameObject[] AddStructureSprites(StructureSO structureSO, Transform target, StructureSO.Dir dir) {
        LoadStructureSprites(structureSO);

        (bool flipped, bool reversed, Texture2D[][] structureTextures) = GetTextures(structures[structureSO.spriteReference], dir);


        int width,length;
        if(dir == StructureSO.Dir.SE || dir == StructureSO.Dir.NW) {
            width  = structureSO.width;
            length = structureSO.length;
        }
        else {
            width  = structureSO.length;
            length = structureSO.width;
        }
        //Debug.Log($"width: {width}, length: {length}");
        
        GameObject[] renderers = new GameObject[4];
        for(int x = 0; x < 2; x++) { //0: Back, 1: Front
            for (int z = 0; z < 2; z++) { //0: Left, 1: Right
                GameObject obj = new GameObject($"StructureSprite {x},{z}");
                renderers[x*2+z] = obj;

                float xPos = (1-x + 0+z) * width / 2.0f; 
                float zPos = (1-x + 1-z) * length / 2.0f;
                float yPos = x==1?-(xPos+zPos): -(width+length)/2.0f;

                obj.transform.position = target.position + new Vector3 (xPos + ((5.12f-target.position.z)*(x-1)/500f), yPos * yPositionUnit, zPos + ((5.12f-target.position.x)*(x-1)/500f))*0.16f;
                obj.transform.parent = target;
                obj.transform.localScale = new Vector3(flipped?-1.0f:1.0f, 0.81f, 1.0f); //i dont know where 0.81 is coming from, i want to know so i can make it a constant calculated from the camera angle!

                SpriteAnimator sa = obj.AddComponent<SpriteAnimator>();                
                if(flipped) sa.Init(structureTextures[x*2+(1-z)],flipped,reversed,structureSO.framesPerSwap);
                else        sa.Init(structureTextures[x*2+z    ],flipped,reversed,structureSO.framesPerSwap);


                obj.GetComponent<Renderer>().material = new Material(structureSpriteMaterial);
            }
        }

        renderers[1].transform.rotation = Quaternion.Euler(0,90,0);
        renderers[2].transform.rotation = Quaternion.Euler(0,90,0);

        return renderers;
    }

    public (bool,bool,Texture2D[][]) GetTextures(Texture2D[][][] structureTextures, StructureSO.Dir dir) {
        return GetTextures(structureTextures,dir,false,false);
    }
    public (bool,bool,Texture2D[][]) GetTextures(Texture2D[][][] structureTextures, StructureSO.Dir dir, bool flipped, bool reverse) {
        int id = StructureSO.DirectionalSpriteID(dir);

        if(id <= structureTextures.Length-1) {
            return (flipped,reverse,structureTextures[id]);
        }
        
        else {
            switch (dir) {
                default: 
                case StructureSO.Dir.SE: 
                    //use debugSprite
                    Debug.Log("WARNING, THERE ARE NO SPRITES FOR THIS STRUCTURE");
                    return (flipped,reverse,null);
                case StructureSO.Dir.NW: 
                    //Debug.Log("No NW sprites, using SE sprites reversed");
                    return GetTextures(structureTextures,StructureSO.Dir.SE,flipped,true);
                case StructureSO.Dir.SW:
                    //Debug.Log("No SW sprites, using SE sprites flipped");
                    return GetTextures(structureTextures,StructureSO.Dir.SE,true,reverse);
                case StructureSO.Dir.NE:
                    //Debug.Log("No NE sprites, using NW sprites flipped");
                    return GetTextures(structureTextures,StructureSO.Dir.NW,true,reverse);
            }
        }
    }

    #region Processing Sprite Textures

    public void LoadStructureSprites(StructureSO structureSO) {
        if (!structures.ContainsKey(structureSO.spriteReference)) structures.Add(structureSO.spriteReference,ProcessStructureSprites(structureSO));
    }

    private Texture2D[][][] ProcessStructureSprites (StructureSO structureSO) {
        int leftRightWidth = structureSO.length * 8;
        int frontBackWidth = structureSO.width * 8;
        int width = leftRightWidth + frontBackWidth;
        int height = structureSO.spriteHeight;
        string spriteReference = structureSO.spriteReference;
        Texture2D spriteSheet = new Texture2D(1,1);
        try {
            spriteSheet.LoadImage(File.ReadAllBytes(Application.streamingAssetsPath + "/StructureSprites/"+spriteReference+".png"));
        } catch (FileNotFoundException e) {
            Debug.Log("Using Debug sprite for " + spriteReference + ". Threw:" + e.ToString());
            //TODO DEBUG SPRITE
            return null;
        }

        int frameCount = spriteSheet.width/width;
        int directions = spriteSheet.height/(2*height);

        Debug.Log($"Loaded spritesheet for {structureSO.structureName} with size ({spriteSheet.width}px,{spriteSheet.height}px)."
                 +$"\n there are sprites for: {directions} directions with {frameCount} frames");

        //Process it row by row. SW -> NW -> SE -> NE
        //Texture2D[][] rows = new (Texture2D,Texture2D)[directions];
        Texture2D[][][] textures = new Texture2D[directions][][];

        for(int d = 0;d<directions;d++) {
            Texture2D[][] faceTextures = new Texture2D[4][];

            int yOffset = spriteSheet.height - ((d+1)*(2*height));

            for (int i = 0; i<2; i++) { //0: Back, 1: Front
                for (int j = 0; j<2; j++) { //0: Left, 1: Right
                    int offsetWidth = j*(i==0?frontBackWidth:leftRightWidth);
                    int frameWidth  =    i==j?frontBackWidth:leftRightWidth;
                    Texture2D[] frameTextures = new Texture2D[frameCount];

                    for(int f = 0;f<frameCount;f++) {
                        Color[] framePixels = spriteSheet.GetPixels(f*width+offsetWidth, yOffset+height-(i*height), frameWidth, height);
                        frameTextures[f] = ProcessFrame(framePixels,frameWidth,height,i!=j);
                    }
                    faceTextures[i*2+j] = frameTextures;
                }
            }
            textures[d] = faceTextures;
        } 

        return textures;       
    }

    private Texture2D ProcessFrame(Color[] pixels, int width, int height, bool upwardsSkew) {
        (Color[] upscaled, int upWidth, int upHeight) = UpscalePixels(pixels,width,height);
        (Color[] skewed, int skWidth, int skHeight) = SkewPixels(upscaled,upWidth,upHeight,upwardsSkew);

        Texture2D tex = new Texture2D(skWidth, skHeight, TextureFormat.RGBA32, false);
        tex.SetPixels(skewed);
        tex.Apply();
        tex.filterMode = FilterMode.Point;

        Rect rect = new Rect(0, 0, tex.width, tex.height);
        Vector2 pivot = new Vector2(0.5f,0.0f);
        return tex;
    }

    private (Color[],int,int) UpscalePixels(Color[] pixels, int width, int height){
        int upscaledWidth = width*upscaleAmount;
        int upscaledHeight = height*upscaleAmount;

        Color[] upscaled = new Color[upscaledWidth*upscaledHeight];
        for(int y = 0; y<height; y++) { 
            for(int x = 0; x<width; x++) {
                Color pixel = pixels[x+y*width];
                for(int yy = 0; yy<upscaleAmount; yy++){
                    for(int xx = 0; xx<upscaleAmount; xx++){
                        upscaled[(x*upscaleAmount+xx) + (y*upscaleAmount+yy)*upscaledWidth] = pixel;
                    }
                }
            }
        }
        return (upscaled,upscaledWidth,upscaledHeight);
    }

    private (Color[],int,int) SkewPixels(Color[] pixels, int width, int height, bool upwardsSkew) {
        int skewHeightOffset = width/2-1;
        int skewedHeight = height+skewHeightOffset;

        int skewY = upwardsSkew? 0:skewHeightOffset;
        int skewDirection = upwardsSkew? 1:-1;

        Color[] skewwed = Enumerable.Repeat(Color.clear,width*skewedHeight).ToArray();
        for(int x = 0; x<width; x++) { 
            for(int y = 0; y<height; y++) {
                skewwed[x+(y+skewY)*width] = pixels[x+y*width];
            }
            skewY += x%2 * skewDirection;
        }
        return (skewwed,width,skewedHeight);
    }

    #endregion

    // private Sprite[][] generateDebugSpriteSheet(StructureSO structureSO) {
    //     int width = structureSO.width;
    //     int length = structureSO.length;
    //     int height = Mathf.Max(width,length);

    //     Sprite[][] sprites = new Sprite[4][];

    //     //set sprite 0 and 1 to empty sprites
    //     sprites[0] = new Sprite[1];       
    //     sprites[1] = new Sprite[0];     

    //     return sprites;
    // }
}