using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour {


    private int frameCount;
    private Texture2D[] textures;
    private MeshRenderer meshRenderer;
    private bool reversed;
    private int framesPerSwap;


    public void Init(Texture2D[] textures, bool flipped, bool reversed, int framesPerSwap) {
        int upscale = SpriteHandler.Instance.upscaleAmount;
        SpawnQuad(textures[0].width/(50.0f*upscale),textures[0].height/(50.0f*upscale));

        this.framesPerSwap = framesPerSwap;
        frameCount = textures.Length;

        this.textures = new Texture2D[frameCount];

        for(int i = 0; i<frameCount;i++) {
            Texture2D texture = textures[reversed?(frameCount-1-i):i];
            this.textures[i] = texture;
        }

        SpriteHandler.Instance.AnimationUpdate += Insant_Animate;
    }

    
    private void OnDestroy() {
        SpriteHandler.Instance.AnimationUpdate -= Insant_Animate;
    }

    private void Insant_Animate(object sender, System.EventArgs e) {
        meshRenderer.material.mainTexture = textures[(SpriteHandler.Instance.animationFrame/framesPerSwap)%frameCount];
    }

    private void SpawnQuad(float width, float height){
        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshFilter.mesh = new Mesh();

        meshFilter.mesh.vertices = new Vector3[] { new Vector3(-width/2,0,0), new Vector3( width/2,0,0), new Vector3( width/2,height,0), new Vector3(-width/2,height,0) };
        meshFilter.mesh.uv = new Vector2[] { new Vector2(0,0), new Vector2(1,0), new Vector2(1,1), new Vector2(0,1) };
        meshFilter.mesh.triangles = new int[] {0,1,2,0,2,3};

        meshFilter.mesh.RecalculateBounds();
        meshFilter.mesh.RecalculateNormals();
    }
}
