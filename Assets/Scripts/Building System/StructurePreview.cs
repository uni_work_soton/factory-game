
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructurePreview : MonoBehaviour {

    GameObject[] validPreviewSprites;
    GameObject[] invalidPreviewSprites;

    public Material validPreviewMaterial;
    public Material invalidPreviewMaterial;


    void Start() {
        validPreviewSprites = new GameObject[0];
        invalidPreviewSprites = new GameObject[0];
        GeneratePreview();

        //add when setup event system
        BuildingSystem.Instance.OnStructureSelected += Instant_ToBuildChanged;
    }

    private void Instant_ToBuildChanged(object sender, System.EventArgs e) {
        GeneratePreview();
    }

    void LateUpdate() {
        UpdatePreview();
    }

    void UpdatePreview() {
        (bool canBuild,Vector3 pos) = BuildingSystem.Instance.PreviewPosition();
        pos.y = 0f;
        transform.position = pos;
        foreach(GameObject obj in validPreviewSprites)   {obj.SetActive(canBuild);}
        foreach(GameObject obj in invalidPreviewSprites) {obj.SetActive(!canBuild);}
    }

    private void GeneratePreview() {
        foreach(GameObject obj in validPreviewSprites) { Destroy(obj); }
        foreach(GameObject obj in invalidPreviewSprites) { Destroy(obj); }
        
        StructureSO toBuild = BuildingSystem.Instance.GetToBuild();
        if(toBuild) {
            validPreviewSprites = SpriteHandler.Instance.AddStructureSprites(toBuild,transform,BuildingSystem.Instance.GetToBuildDir());
            invalidPreviewSprites = SpriteHandler.Instance.AddStructureSprites(toBuild,transform,BuildingSystem.Instance.GetToBuildDir());
            foreach(GameObject obj in validPreviewSprites)   { obj.GetComponent<Renderer>().material = validPreviewMaterial; }
            foreach(GameObject obj in invalidPreviewSprites) { obj.GetComponent<Renderer>().material = invalidPreviewMaterial; }
        } else {
            validPreviewSprites = new GameObject[0]; 
            invalidPreviewSprites = new GameObject[0]; 
        }
        UpdatePreview();
    }
}
