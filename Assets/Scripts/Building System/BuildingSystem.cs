using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/* This class is responsible for handling:
 * which structure to build/get which structure for 
 * what structures have been built
 * where those structures have been built
 */
public class BuildingSystem : MonoBehaviour {

    public static BuildingSystem Instance { get; private set; }

    private enum BuildingMode {
        VIEW,
        PLACE,
        DELETE,
    }
    private BuildingMode buildingMode;

    public event EventHandler OnStructureSelected;
    public event EventHandler UpdateTutorialFunctions;
    private bool updatedTutorial = false;

    public List<StructureSO> buildableStructures;

    private List<Structure> placedStructures;
    private Structure[,] structurePositions;

    private StructureSO toBuild;
    private Structure toRemove;
    private StructureSO.Dir dir;

    public GridLayout gridLayout;
    private Grid grid;

    public GameObject logicPlane;

    public int factoryWidth;
    public int factoryLength;

    public AudioClip place;
    public AudioClip remove;
    public AudioClip cantPlace;

    
    private void Awake() {
        Instance = this;

        grid = gridLayout.gameObject.GetComponent<Grid>();

        buildingMode = BuildingMode.PLACE;

        placedStructures = new List<Structure>();
        structurePositions = new Structure[factoryWidth,factoryLength];

        dir = StructureSO.Dir.SE;


    }

    private void Start() {
        RefreshPreview();
    }


    private void Update() {
        //TODO - Make a proper input handler class for this and make building selection drag and drop
        if (Input.GetKeyDown(KeyCode.C)){
            BlockManager.Instance.ClearFactory();
        }
        if (Input.GetKeyDown(KeyCode.Escape)){
            SwitchToViewMode();
        }
        if (Input.GetKeyDown(KeyCode.X)){
            SwitchToDeleteMode();
        }     
        
        //rotation of object being built
        if (Input.GetKeyDown(KeyCode.Q)){ //↶ counter clockwise
            dir = StructureSO.RotLeft(dir);
            OnStructureSelected?.Invoke(this, EventArgs.Empty);
        }
        if (Input.GetKeyDown( KeyCode.E)){ //↷ clockwise
            dir = StructureSO.RotRight(dir);
            OnStructureSelected?.Invoke(this, EventArgs.Empty);
        }

        if (Input.GetMouseButtonDown(0)){
            ClickOnTile();
        } else {
            MouseOverTile();
        }
    }   

    public void SwitchToViewMode(){
        UnsetToBuild();
        UnsetToRemove();
        buildingMode = BuildingMode.VIEW;
    }
    public void SwitchToPlaceMode(){
        UnsetToRemove();
        buildingMode = BuildingMode.PLACE;
    }
    public void SwitchToDeleteMode(){
        UnsetToBuild();
        buildingMode = BuildingMode.DELETE;
    }

    private void ClickOnTile() {
        //TODO
        switch(buildingMode) {
            case BuildingMode.VIEW:
                break;
            case BuildingMode.PLACE:
                MousePlace(); break;
            case BuildingMode.DELETE:
                MouseRemove(); break;
        }
    }

    private void MouseOverTile() {
        //TODO
        switch(buildingMode) {
            case BuildingMode.VIEW:
                break;
            case BuildingMode.PLACE:
                break;
            case BuildingMode.DELETE:
                HighlightRemoval(); break;
        }
    }


    //have these functions called by the input handler when u make it, 
    #region Mouse Inputs 
    private void MousePlace() {
        if(!toBuild) return;
        PlaceStructure(MouseCellPos(),dir,toBuild);
    }

    private void MouseRemove() {
        if(!toRemove) return;
        if (RemoveStructureAt(MouseCellPos())) {
            //play sound
            remove = Resources.Load<AudioClip>("Sounds/destroy");
            AudioSource.PlayClipAtPoint(remove, new Vector3(-10,10,-10),0.7f); 
        } 
  
    }

    private void HighlightRemoval() {
        Vector2Int target = MouseCellPos();
        Structure structure;
        if(!new Rect(0,0,factoryWidth,factoryLength).Contains(target) || !(structure = structurePositions[target.x,target.y])) {
            toRemove?.SetToRemove(false);
            toRemove = null;
            return;
        }

        if (structure && structure != toRemove) {
            structure.SetToRemove(true);
            UnsetToRemove();
            toRemove = structure;
        }
    }

    #endregion

    #region Building Functions
    private void RefreshPreview(){
        OnStructureSelected?.Invoke(this, EventArgs.Empty);
    }

    public Vector2Int ClampPos(Vector2Int pos, StructureSO.Dir dir, StructureSO targetStructure) {
        (Vector2Int min, Vector2Int max) = targetStructure.GetPlacementBounds(dir);
        pos.Clamp( new Vector2Int( -min.x, -min.y ), new Vector2Int( factoryWidth-1-max.x, factoryLength-1-max.y ) );
        return pos;
    }

    public Structure PlaceStructure(Vector2Int tile, StructureSO.Dir dir, StructureSO toBuild ) {
        //if the user is pointing too far outside the grid, not let them place
        if(!new Rect(-4,-4,factoryWidth+3,factoryLength+3).Contains(tile)) {
            return null;
        }

        //bring target position to a tile that would allow all solid tiles to be on the grid
        tile = ClampPos(tile,dir,toBuild);
        
        //check if tile can be placed on the grid.
        if (!CanPlaceStructure(tile,dir,toBuild)) {
            //play unsuccessful place sound
            cantPlace = Resources.Load<AudioClip>("Sounds/cantPlace");
            AudioSource.PlayClipAtPoint(cantPlace, new Vector3(-10,10,-10),0.7f);
            return null;
        }
        
        //check if the player has the valid resources to place
        if (!StructureHandler.Instance.TryBuildStructure(toBuild, true)) {
            //play unsuccessful place sound
            cantPlace = Resources.Load<AudioClip>("Sounds/cantPlace");
            AudioSource.PlayClipAtPoint(cantPlace, new Vector3(-10,10,-10),0.7f);
            return null;
        }
        

        //place the structure and add it to the reference data structures
        Structure placed = Structure.CreateStructure(tile,TileToWorld(tile),dir,toBuild);
        placedStructures.Add(placed);
        foreach(Vector2Int tilepos in toBuild.GetSolidTiles(tile,dir)) {
             structurePositions[tilepos.x,tilepos.y] = placed;
        }
        
        //play successful place sound
        place = Resources.Load<AudioClip>("Sounds/place");
        AudioSource.PlayClipAtPoint(place, new Vector3(-10,10,-10),0.7f);

        //return structure so functions trying to place know it succeeded
        return placed;
    }

    private bool CanPlaceStructure(Vector2Int tile, StructureSO.Dir dir, StructureSO toBuild) {
        if (TutorialHandler.Instance.currentStep.structurePlacementRequirements != null) {
            if (!TutorialHandler.Instance.SatisfiesCurrentPlacementRequirements(tile, dir, toBuild)) {
                if (!updatedTutorial) return false;
                updatedTutorial = false;
                UpdateTutorialFunctions?.Invoke(this, EventArgs.Empty);
                return false;
            }

            if (!updatedTutorial) {
                updatedTutorial = true;
                UpdateTutorialFunctions?.Invoke(this, EventArgs.Empty);
            }
        }
        else {
            
            updatedTutorial = false;

            if (!updatedTutorial) {
                updatedTutorial = true;
            }
        }
        
        //check the solid tiles are all able being placed on empty, unoccupied tiles.
        Rect grid = new Rect(0,0,factoryWidth,factoryLength);
        List<Vector2Int> solidTiles = toBuild.GetSolidTiles(tile,dir);
        return !solidTiles.Any(tilepos => !grid.Contains(tilepos) || structurePositions[tilepos.x,tilepos.y]) && StructureHandler.Instance.TryBuildStructure(toBuild, false);
    }

    public bool RemoveStructureAt(Vector2Int tile) {
        Structure structure;
        if (!(structure = GetStructureAt(tile))) {
            return false;
        }
        toRemove = null;
        StructureHandler.Instance.StoreStructure(structure.structureSO,1);
        placedStructures.Remove(structure);
        foreach(Vector2Int tilePos in structure.GetSolidTiles()) {
            structurePositions[tilePos.x,tile.y] = null;
        }
        Destroy(structure.gameObject);    

        return true;
    }
    
    #endregion

    #region Positions

    public Vector3 MouseWorldPos(){
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (logicPlane.GetComponent<Collider>().Raycast(ray, out RaycastHit raycastHit,100.0f)){
            return (raycastHit.point + new Vector3(-6f,0f,1.2f)) * 0.5f;
        }
        else {
            return Vector3.zero;
        }
    }

    public Vector2Int MouseCellPos(){ 
        return WorldToTile(MouseWorldPos());
    }
    public Vector2Int WorldToTile(Vector3 worldPos) {
        Vector3Int cellPos = gridLayout.WorldToCell(worldPos);
        return new Vector2Int(cellPos.x,cellPos.y);
    }

    public Vector3 TileToWorld(Vector2Int tilePos){
        Vector3 tileWorldPos = grid.GetCellCenterWorld(new Vector3Int(tilePos.x,tilePos.y,0));
        tileWorldPos -= new Vector3(0.08f,0f,0.08f); //(moves to bottom corner of cell)
        return tileWorldPos;
    }

    public (bool,Vector3) PreviewPosition() {
        if (!toBuild) return (false,new Vector3(0f,0f,0f));

        Vector2Int tile = MouseCellPos();
        Vector2Int clamped = ClampPos(tile,dir,toBuild);

        Rect mouseBounds = new Rect(-4,-4,factoryWidth+3,factoryLength+3);

        return ( 
            mouseBounds.Contains(tile) && CanPlaceStructure(clamped,dir,toBuild), 
            TileToWorld(clamped)
        );
    }

    #endregion

    #region getters setters

    

    private void UnsetToRemove() {
        toRemove?.SetToRemove(false);
        toRemove = null;
    }

    public void UnsetToBuild() {
        toBuild = null;
        RefreshPreview();
    }

    public void SetToBuild(StructureSO toBuild) {
        this.toBuild = toBuild;
        SwitchToPlaceMode();
        RefreshPreview();
    }    

    public StructureSO GetToBuild() {
        return toBuild;
    }

    public Quaternion GetToBuildAngle() {
        if(toBuild) {
            return Quaternion.Euler(0,StructureSO.GetRotAngle(dir),0);
        }
        else {
            return Quaternion.identity;
        }
    }
    
    public StructureSO.Dir GetToBuildDir() {
        return dir;
    }

    public Structure GetStructureAt(Vector2Int tile) {
        Structure structure;
        if(!new Rect(0,0,factoryWidth,factoryLength-1).Contains(tile) || !(structure = structurePositions[tile.x,tile.y])) {
            return null;
        }
        return structure;
    }
    #endregion
}