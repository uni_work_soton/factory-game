using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPlacementPreview : MonoBehaviour {

    GameObject[] tutorialPreviewSprites;

    public Material tutorialPlacementMaterial;
    private Nullable<TutorialHandler.StructurePlacementRequirements> currentRequirements;


    void Start() {
        tutorialPreviewSprites = new GameObject[0];
        GeneratePreview();

        //add when setup event system
        TutorialHandler.Instance.OnTutorialStepChanged += Instant_GeneratePreview;
        BuildingSystem.Instance.OnStructureSelected += Instant_GeneratePreview;
    }

    private void Instant_GeneratePreview(object sender, System.EventArgs e) {
        GeneratePreview();
    }

    void LateUpdate() {
        UpdatePreview();
    }

    void UpdatePreview() {
        if (currentRequirements == null) return;
        
        Vector2Int pos = BuildingSystem.Instance.MouseCellPos();
        StructureSO.Dir dir = BuildingSystem.Instance.GetToBuildDir();


        if (pos == currentRequirements.Value.position && currentRequirements.Value.rotations.Contains(dir)) {
            foreach(GameObject obj in tutorialPreviewSprites)  {obj.SetActive(false);}
        } else {
            foreach(GameObject obj in tutorialPreviewSprites)  {obj.SetActive(true);}
        }
    }

    private void GeneratePreview() {
        foreach(GameObject obj in tutorialPreviewSprites) { Destroy(obj); }
        tutorialPreviewSprites = new GameObject[0];
        
        if ((currentRequirements = TutorialHandler.Instance.currentStep.structurePlacementRequirements) != null)  {
            if (currentRequirements.Value.structure == BuildingSystem.Instance.GetToBuild()) {
                transform.position = BuildingSystem.Instance.TileToWorld(currentRequirements.Value.position);

                tutorialPreviewSprites = SpriteHandler.Instance.AddStructureSprites(
                    currentRequirements.Value.structure,
                    transform,
                    currentRequirements.Value.rotations[0]
                );

                foreach (GameObject obj in tutorialPreviewSprites) {
                    obj.GetComponent<Renderer>().material = tutorialPlacementMaterial;
                }
            }
        }

        UpdatePreview();
    }
}
