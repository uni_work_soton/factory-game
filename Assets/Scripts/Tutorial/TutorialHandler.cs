using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHandler : MonoBehaviour {

    public static TutorialHandler Instance;

    [SerializeField] private StructureSO basicConveyor;
    [SerializeField] private StructureSO quartzSpawner;
    [SerializeField] private StructureSO basicProcessor;
    [SerializeField] private StructureSO arcaneInfuser;

    private bool skippedTutorial;

    public event EventHandler OnTutorialStepChanged;

    private List<string> completedSteps;
    private Queue<TutorialStep> tutorialSteps; 
    public TutorialStep currentStep;


    public struct StructurePlacementRequirements {
        public StructureSO structure;
        public Vector2Int position;
        public List<StructureSO.Dir> rotations;

        public StructurePlacementRequirements(Vector2Int pos, List<StructureSO.Dir> rots, StructureSO struc) {
            position = new Vector2Int(pos.x,pos.y);
            rotations = rots;
            structure = struc;
        }
    }

    public struct TutorialStep {
        public string stepName; 
        public Func<bool> stepCheck;
        public StructurePlacementRequirements? structurePlacementRequirements;

        public TutorialStep(string stepName, Func<bool> stepCheck,  StructurePlacementRequirements? structurePlacementRequirements = null) {
            this.stepName = stepName;
            this.stepCheck = stepCheck;
            this.structurePlacementRequirements = structurePlacementRequirements;
        }

    }


    private void Start() {
        Instance = this;

        skippedTutorial = false;

        completedSteps = new List<string>();
        tutorialSteps = new Queue<TutorialStep>();

        
        Vector2Int pos = new Vector2Int(20,22);

        float time = Time.time;

        tutorialSteps.Enqueue( new TutorialStep(
            "Wait for Sprites to Load", 
            () => Time.time > time + 0.25f
        ));

        StructurePlacementRequirements spawnerR =
            new StructurePlacementRequirements(pos, new List<StructureSO.Dir> { StructureSO.Dir.SE }, quartzSpawner);
        StructureHandler.Instance.StoreStructure(quartzSpawner,1);
        tutorialSteps.Enqueue( new TutorialStep(
            "Place Spawner", 
            () => CheckSpecificPlacement(spawnerR),
            spawnerR
        ));

        pos += new Vector2Int(1,-2);

        for(int i = 0; i < 4; i++) {
            StructurePlacementRequirements conveyorR = new StructurePlacementRequirements(pos,
                new List<StructureSO.Dir> { StructureSO.Dir.SW }, basicConveyor);
            StructureHandler.Instance.StoreStructure(basicConveyor,1);
            tutorialSteps.Enqueue( new TutorialStep(
                $"Place Conveyor {i}", 
                () => CheckSpecificPlacement(conveyorR),
                conveyorR
            ));
            pos += new Vector2Int(-2,0);
        }

        StructurePlacementRequirements processorR = new StructurePlacementRequirements(pos,
            new List<StructureSO.Dir>
                { StructureSO.Dir.SE, StructureSO.Dir.SW, StructureSO.Dir.NE, StructureSO.Dir.NW }, basicProcessor);
        StructureHandler.Instance.StoreStructure(basicProcessor,1);
        tutorialSteps.Enqueue( new TutorialStep(
            "Place Processor", 
            () => CheckSpecificPlacement(processorR),
            processorR
        ));

        tutorialSteps.Enqueue( new TutorialStep(
            "Enable Spawning", 
            () => BlockManager.Instance.spawningEnabled == true
        ));

        pos += new Vector2Int(2,-3);
        tutorialSteps.Enqueue( new TutorialStep(
            "Wait till cubits for infuser", 
            () => CurrencyManager.Instance.GetMoney() > 75
        ));

        StructurePlacementRequirements infuserR = new StructurePlacementRequirements(pos,
            new List<StructureSO.Dir> { StructureSO.Dir.NE, StructureSO.Dir.SW }, arcaneInfuser);
        tutorialSteps.Enqueue( new TutorialStep(
            "Place Infuser", 
            () => CheckSpecificPlacement(infuserR),
            infuserR
        ));
        tutorialSteps.Enqueue( new TutorialStep(
            "Finish", 
            () => CurrencyManager.Instance.GetMoney() > 30
        ));

        tutorialSteps.Enqueue( new TutorialStep(
            "Tutorial Finished!", 
            () => false
        ));
        

        currentStep = tutorialSteps.Dequeue();
    }

    private bool CheckSpecificPlacement(StructurePlacementRequirements? structurePlacementRequirements) {
        if (structurePlacementRequirements == null) return true;
        StructurePlacementRequirements reqs = structurePlacementRequirements.Value;
        Structure toCheck;
        if ((toCheck = BuildingSystem.Instance.GetStructureAt(reqs.position)) == null) return false;
        return SatisfiesPlacementRequirements(reqs, toCheck.tile, toCheck.dir, toCheck.structureSO);
    }

    public bool SatisfiesCurrentPlacementRequirements(Vector2Int position, StructureSO.Dir dir, StructureSO structureSO) {
        StructurePlacementRequirements? reqs;
        return (reqs = currentStep.structurePlacementRequirements) == null || SatisfiesPlacementRequirements(reqs.Value, position, dir, structureSO);
    }
    private bool SatisfiesPlacementRequirements(StructurePlacementRequirements requirements, Vector2Int position,
        StructureSO.Dir dir, StructureSO structureSO) {
        
        return structureSO == requirements.structure && requirements.rotations.Contains(dir) &&
               position == requirements.position;
        
    }

    private void Update() {
        if (skippedTutorial) return;
        if (currentStep.stepCheck.Invoke()) {
            Debug.Log($"Player completed tutorial step: {currentStep.stepName}");
            
            completedSteps.Add(currentStep.stepName);
            currentStep = tutorialSteps.Dequeue();

            OnTutorialStepChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
