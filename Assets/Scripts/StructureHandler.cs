using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script is mainly just a container object for the Building System and the UI to interact with.
public class StructureHandler : MonoBehaviour {

    public static StructureHandler Instance;
    public List<StructureSO> conveyors;
    public List<StructureSO> crystallizers;
    public List<StructureSO> processors;
    public List<StructureSO> infusers;
    
    public Dictionary<StructureSO,int> storage;

    public bool readyToPlace;

    private void Awake() {
        StructureHandler.Instance = this;
        readyToPlace = false;
        
        storage = new Dictionary<StructureSO,int>();

        AddListToStorage(crystallizers);
        AddListToStorage(conveyors);
        AddListToStorage(processors);
        AddListToStorage(infusers);
    }

    private void Start() {
        foreach(KeyValuePair<StructureSO, int> struc in storage) {
            SpriteHandler.Instance.LoadStructureSprites(struc.Key);
        }
        readyToPlace = true;
    }

    private void AddListToStorage(List<StructureSO> l){
        l.ForEach(structureSO => storage.Add(structureSO,0));
    }

    public void SelectStructureToBuild(StructureSO toBuild){
        BuildingSystem buildingSystem = BuildingSystem.Instance;
        //tell building system to build the structureSO the player selected in the UI
    }

    public bool TryBuildStructure(StructureSO toBuild, bool consume) {
        if (GetStorageCount(toBuild) > 0) {
            if (consume) { UseStoredStructure(toBuild, 1);}
            return true;
        }

        if (CurrencyManager.Instance.GetMoney() >= toBuild.structureCost) {
            if (consume) { CurrencyManager.Instance.SubtractMoney(toBuild.structureCost);}
            return true;
        }
        
        return false;
    }

    public void StoreStructure(StructureSO structureSO, int count) {
        storage[structureSO] += count;
        Debug.Log($"Added {count} {structureSO.structureName}s to Storage. There are now {storage[structureSO]} stored");
    }
    public void UseStoredStructure(StructureSO structureSO, int count) {
        storage[structureSO] -= count;
        Debug.Log($"Removed {count} {structureSO.structureName}s to Storage. There are now {storage[structureSO]} stored");
    }

    public int GetStorageCount(StructureSO structureSO) {
        return storage[structureSO];
    }

    public bool IsConveyor(StructureSO structureSO) {
        return conveyors.Contains(structureSO);
    }
    public bool IsCrystallizer(StructureSO structureSO) {
        return crystallizers.Contains(structureSO);
    }
    public bool IsUpgrader(StructureSO structureSO) {
        return infusers.Contains(structureSO);
    }
    public bool IsProcessor(StructureSO structureSO) {
        return processors.Contains(structureSO);
    }
}
