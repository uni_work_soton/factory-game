using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Array2DEditor;

[CreateAssetMenu(fileName = "New Structure", menuName = "ScriptableObjects/Structure")]

public class StructureSO : ScriptableObject {

    public enum Dir {
        NW,SW,SE,NE
    }


    public string structureName;
    public long structureCost;
    [TextArea(3,10)]
    public string structureDescription;
    public Sprite structureIcon;

    [Tooltip("Make sure the structure's assets in the resources folder use the same name")]
    public string spriteReference;
    public int spriteHeight;

    [Tooltip("Game runs animations at 24fps, this frames per swap is how many of those 24 frames before it swaps.\n Try keep amount of frames * frames per swap a factor of 24")]
    public int framesPerSwap;

    [HideInInspector] public int length = 2; //↖↘
    [HideInInspector] public int width = 2;  //↗↙
    [HideInInspector] public int height; //↑↓ //not used (yet)

    public Transform hitbox;

    public Array2DBool placementGrid;
    public List<ComponentSO> componentBehaviours;

    private void OnValidate() {
        width = placementGrid.GridSize.x;
        length = placementGrid.GridSize.y;
    }

    public static string ToText(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    return "NW";
            case Dir.SW:    return "SW";
            case Dir.SE:    return "SE";
            case Dir.NE:    return "NE";
        }
    }

    public static Dir RotLeft(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    return Dir.SW;
            case Dir.SW:    return Dir.SE;
            case Dir.SE:    return Dir.NE;
            case Dir.NE:    return Dir.NW;
        }
    }

    public static Dir RotRight(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    return Dir.NE;
            case Dir.SW:    return Dir.NW;
            case Dir.SE:    return Dir.SW;
            case Dir.NE:    return Dir.SE;
        }
    }

    public static float GetRotAngle(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    return 0f;
            case Dir.SW:    return 270f;
            case Dir.SE:    return 180f;
            case Dir.NE:    return 90f;
        }
    }

    public static int DirectionalSpriteID(Dir dir) {
        switch (dir) {
            default:
            case Dir.SE:    return 0;
            case Dir.NW:    return 1;
            case Dir.SW:    return 2;
            case Dir.NE:    return 3;
        }
    }

    public Vector2Int GetRotOffset(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    return new Vector2Int(0, 0);
            case Dir.SW:    return new Vector2Int(length, 0);
            case Dir.SE:    return new Vector2Int(width, length);
            case Dir.NE:    return new Vector2Int(0, width);
        }
    }

    public Vector2Int GetRotatedDimensions(Dir dir) {
        switch (dir) {
            default:
            case Dir.NW:    case Dir.SE:    return new Vector2Int(width, length);
            case Dir.SW:    case Dir.NE:    return new Vector2Int(length, width);
        }
    }

    //returns the minimum size rectangle that contains all solid squares
    public (Vector2Int,Vector2Int) GetPlacementBounds(Dir dir) {
        List<Vector2Int> solidTiles = GetSolidTiles(new Vector2Int(0,0),dir);
        int maxX = -1;
        int maxY = -1;
        int minX = 100;
        int minY = 100;
        foreach(Vector2Int TilePos in solidTiles){
            maxX = Mathf.Max(maxX,TilePos.x);
            maxY = Mathf.Max(maxY,TilePos.y);
            minX = Mathf.Min(minX,TilePos.x);
            minY = Mathf.Min(minY,TilePos.y);
        }
        return (new Vector2Int(minX,minY),new Vector2Int(maxX,maxY));
    }

    
    public List<Vector2Int> GetSolidTiles(Vector2Int pos, Dir dir) {
        List<Vector2Int> solidTiles = new List<Vector2Int>();

        for(int x = 0; x < width; x++) {
            for(int y = 0; y < length; y++) {
                if (placementGrid.GetCell(x,y)) {
                    switch (dir) {
                        default:
                        case Dir.NW:    solidTiles.Add(pos + new Vector2Int((width-1)-x,(length-1)-y));    break;
                        case Dir.SW:    solidTiles.Add(pos + new Vector2Int(y,(width-1)-x));               break;
                        case Dir.SE:    solidTiles.Add(pos + new Vector2Int(x,y));                         break;
                        case Dir.NE:    solidTiles.Add(pos + new Vector2Int((length-1)-y,x));              break;
                    }
                }
            }
        }

        return solidTiles;
    }  


}
