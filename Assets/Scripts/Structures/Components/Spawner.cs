using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public static void AddSpawnerBehaviour(GameObject obj, SpawnerSO spawnerSO) {
        Spawner spawner = obj.AddComponent<Spawner>();
        spawner.spawnedItem = spawnerSO.spawnedItem;
        spawner.ticksPerSpawn = spawnerSO.ticksPerSpawn;

    }

    public BlockSO spawnedItem;
    public int ticksPerSpawn;
    private int ticksTillSpawn;

    private void Start() {
        ticksTillSpawn = ticksPerSpawn;
    }

    private void SpawnItem() {
        BlockManager.Instance.SpawnBlock(spawnedItem,transform.position);
    }

    protected void FixedUpdate() {
        if (ticksTillSpawn > 0) {
            ticksTillSpawn -= 1;
        } 
        else {
            SpawnItem();
            ticksTillSpawn = ticksPerSpawn;
        }        
    }
}
