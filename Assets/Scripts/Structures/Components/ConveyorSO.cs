using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Conveyor Behaviour", menuName = "ScriptableObjects/Component Behaviour/Conveyor Behaviour")]
public class ConveyorSO : ComponentSO {
    public float conveyorSpeed;
    public PhysicMaterial conveyorPhysics;

    public override void AddBehaviourToObject(GameObject obj){
        Conveyor.AddConveyorBehaviour(obj,this);
    }
}
