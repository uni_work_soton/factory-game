using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effector Behaviour", menuName = "ScriptableObjects/Component Behaviour/Effector Behaviour")]
public class EffectorSO : ComponentSO {
    
    public List<BlockEffectFactory> onContactEffects;

    public override void AddBehaviourToObject(GameObject obj){
        Effector.AddEffectorBehaviour(obj,this);
    }
}
