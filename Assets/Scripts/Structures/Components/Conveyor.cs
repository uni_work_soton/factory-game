using UnityEngine;

public class Conveyor : MonoBehaviour {

    public static void AddConveyorBehaviour(GameObject obj, ConveyorSO conSO) {
        Conveyor con = obj.AddComponent<Conveyor>();
        con.speed = conSO.conveyorSpeed;
        con.rBody = obj.GetComponent<Rigidbody>();
        con.GetComponent<Collider>().material = conSO.conveyorPhysics;
    }
    
    public float speed;
    protected Rigidbody rBody;

    private void FixedUpdate() {
        //This physics is kind of jank but basically sets the position of the conveyor back, then moves it forwards. 
        //Physics objects ontop dont get moved when we move conveyor with .position but do with .MovePosition().
        //Effectively its using friction to push the blocks every physics update.

        Vector3 pos = rBody.position;
        Vector3 newPos = -transform.forward * speed * Time.fixedDeltaTime;
        //Slightly push objects up a little to avoid them wierdly colliding on the side of flush edges
        newPos += new Vector3 (0,-0.01f,0);
        rBody.position += newPos;
        rBody.MovePosition(pos);
    }

    public void SetSpeed(float speed) {
        this.speed = speed;
    }
}
