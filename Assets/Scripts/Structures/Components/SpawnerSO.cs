using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spawner Behaviour", menuName = "ScriptableObjects/Component Behaviour/Spawner Behaviour")]
public class SpawnerSO : ComponentSO {
    public BlockSO spawnedItem; 
    [Tooltip("Game runs at 50 ticks a second, spawn rate is how many ticks have to pass before spawning")]
    public int ticksPerSpawn;

    public override void AddBehaviourToObject(GameObject obj){
        Spawner.AddSpawnerBehaviour(obj,this);
    }
}
