using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effector : MonoBehaviour{

    public static void AddEffectorBehaviour(GameObject obj, EffectorSO EffectorSO) {
        Effector effector = obj.AddComponent<Effector>();
        effector.OnContactEffects = EffectorSO.onContactEffects;
    }

    protected List<BlockEffectFactory> OnContactEffects;

    void Start() {
       // collisions = new List<GameObject>();
    }

    void OnTriggerEnter(Collider col) {
        if(col.gameObject.tag == "BeltItem") {
            Block colBlock = col.gameObject.GetComponent<Block>();
            OnContactEffects.ForEach(factory => factory.GetBlockEffect(colBlock).ApplyTouch(transform));
        }
    }

    void OnTriggerStay(Collider col) {
        if(col.gameObject.tag == "BeltItem") {
            Block colBlock = col.gameObject.GetComponent<Block>();
            OnContactEffects.ForEach(factory => factory.GetBlockEffect(colBlock).ApplyConstant(transform));
        }
    }
}
