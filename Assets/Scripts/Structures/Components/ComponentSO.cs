using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//currently just used for grouping, could maybe add functionality into it in future?   
//maybe stuff in here for pausing and resuming the effects of components?
[System.Serializable]
public abstract class ComponentSO : ScriptableObject {
    public string tag;
    
    public abstract void AddBehaviourToObject(GameObject obj);
}
