using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
    This class is primarily a container for information relevant to structures that have already been built, like:
    their direction
    which structure they are
    which tile their origin is on
 */
public class Structure : MonoBehaviour {

    public StructureSO structureSO;
    public Vector2Int tile;
    public StructureSO.Dir dir;
    private GameObject[] defaultSprites;
    private GameObject[] removalSprites;

    public static Structure CreateStructure(Vector2Int tile, Vector3 pos, StructureSO.Dir dir, StructureSO toBuild) {
        GameObject structureObj = new GameObject(toBuild.structureName);
        structureObj.transform.position = pos; 
        structureObj.transform.rotation = Quaternion.identity;

        //create the structure object
        Structure structure = structureObj.AddComponent<Structure>();
        structure.structureSO = toBuild;
        structure.tile = tile;
        structure.dir = dir;

        //add its hitbox
        Vector2Int hitboxOffset = toBuild.GetRotOffset(dir);
        Transform hitbox = Instantiate(
            toBuild.hitbox, 
            pos + new Vector3(hitboxOffset.x,0,hitboxOffset.y) * 0.16f, 
            Quaternion.Euler(0f,StructureSO.GetRotAngle(dir),0f),
            structureObj.transform
        );

        //add functionality to its hitbox
        foreach(ComponentSO c in toBuild.componentBehaviours) {
            ApplyComponentBehaviours(hitbox.gameObject,c);
        }

        //spawn the structure graphics
        structure.defaultSprites = new GameObject[0];
        structure.removalSprites = new GameObject[0];
        structure.SpawnSprites();

        return structure;
    }

    private static void ApplyComponentBehaviours(GameObject obj, ComponentSO c) {
        if(obj.tag == c.tag) {
            c.AddBehaviourToObject(obj);
        }
        foreach(Transform child in obj.transform) {
            ApplyComponentBehaviours(child.gameObject,c);
        }

    }

    public void SetToRemove(bool toRemove) {
        foreach(GameObject obj in defaultSprites) { obj.SetActive(!toRemove); }
        foreach(GameObject obj in removalSprites) { obj.SetActive(toRemove); }
        return;
    }

    public Vector2Int GetTile() {
        return tile;
    }

    public List<Vector2Int> GetSolidTiles() {
        return structureSO.GetSolidTiles(tile, dir);
    }
    
    public void SpawnSprites() {
        foreach(GameObject obj in defaultSprites) { Destroy(obj); }
        foreach(GameObject obj in removalSprites) { Destroy(obj); }

        defaultSprites = SpriteHandler.Instance.AddStructureSprites(structureSO,transform,dir);
        removalSprites = SpriteHandler.Instance.AddStructureSprites(structureSO,transform,dir);
        
        foreach(GameObject obj in removalSprites) { 
            obj.GetComponent<Renderer>().material.color = Color.yellow;
            obj.SetActive(false);
        }
    }
}
