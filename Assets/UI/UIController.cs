using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIController : MonoBehaviour {
    //Its insane how similar to java this is....
    //That is so true bestie

    private float percentageOfBlocksUsed;
    
    //Gui elements
    //Top
    private Label currency;
    private Button clearFloor;
    public RadioButton deleteModeRadioButton;
    public RadioButton placeModeRadioButton;
    public Label itemName;
    public Label itemDescription;
    public Label itemCost;
    public Label factoryHaltingLabel;
    public Button factoryEnabledButton;


    //Middle
    private Label progressBarLabel;
    private VisualElement factoryFullnessOuter;
    private VisualElement factoryFullnessBar;

    //Bottom - Menu + Tabs
    private Label tutorialPrompt;
    private VisualElement structureMenu;
    private Button conveyorsTabButton;
    private Button spawnersTabButton;
    private Button upgradersTabButton;
    private Button collectorsTabButton;
        
    //audio thingies!
    public AudioClip tick;
    public AudioSource audioSource;

    //structure list stuff - Ryn
    public VisualTreeAsset structureListEntryTemplate;
    private StructureListController structureListController;
    private Button selectedTab;
    
    //root ui element
    public VisualElement root;

    
    // Start is called before the first frame update
    void Start() {

        //component audio needs to go through
        audioSource = GetComponent<AudioSource>();
        BuildingSystem.Instance.OnStructureSelected += Instant_ToBuildChanged;
        BuildingSystem.Instance.UpdateTutorialFunctions += Instant_UpdateTutorialFunctions;
        TutorialHandler.Instance.OnTutorialStepChanged += Instant_TutorialStepCompleted;

        //UI
        root = GetComponent<UIDocument>().rootVisualElement;

        //Top
        currency = root.Q<Label>("currency");
        clearFloor = root.Q<Button>("clearFactoryFloor");
        itemName = root.Q<Label>("itemName");
        itemDescription = root.Q<Label>("itemDescription");
        itemCost = root.Q<Label>("itemCost");
        factoryHaltingLabel = root.Q<Label>("factoryHaltingLabel");
        factoryEnabledButton = root.Q<Button>("enableSpawning");

        factoryEnabledButton.clicked += factoryEnabledButtonPressed;

        RefreshItemInfo();
        
        //Middle
        progressBarLabel = root.Q<Label>("fullnessLabel");
        factoryFullnessOuter = root.Q<VisualElement>("factoryFullness");
        factoryFullnessBar = root.Q<VisualElement>("fullnessBar");
        
        //Bottom - Menu + Tabs
        tutorialPrompt = root.Q<Label>("tutorialPrompt");
        tutorialPrompt.text = "";
        structureMenu = root.Q<VisualElement>("structureMenu");
        conveyorsTabButton = root.Q<Button>("conveyorTab");
        spawnersTabButton = root.Q<Button>("spawnerTab");
        upgradersTabButton = root.Q<Button>("upgraderTab");
        collectorsTabButton = root.Q<Button>("collectorTab");
        
        //Tab button method assignment
        conveyorsTabButton.clicked += conveyorsTabButtonPressed;
        spawnersTabButton.clicked += spawnersTabButtonPressed;
        upgradersTabButton.clicked += upgradersTabButtonPressed;
        collectorsTabButton.clicked += collectorsTabButtonPressed;

        //Clear Button
        clearFloor.clicked += clearFactoryFloorButtonPressed;

        //Default values
        percentageOfBlocksUsed = 0;
        factoryHaltingLabel.visible = false;

        //setup structure selector
        structureListController = new StructureListController();
        structureListController.SetupStructureList(root,structureListEntryTemplate);
    }
    

    //Tab button methods
    void conveyorsTabButtonPressed() {
        TabButtonPressed(0,new StyleColor(new Color32(86,123,121,255)),conveyorsTabButton);
    }

    void spawnersTabButtonPressed() {
        TabButtonPressed(1,new StyleColor(new Color32(146,93,159,255)),spawnersTabButton);
    }

    void upgradersTabButtonPressed() {
        TabButtonPressed(2,new StyleColor(new Color32(101,39,42,255)),upgradersTabButton);
    }

    void collectorsTabButtonPressed() {
        TabButtonPressed(3,new StyleColor(new Color32(123,114,67,255)),collectorsTabButton);
    }

    private void TabButtonPressed(int i, StyleColor color, Button button) {
        if (selectedTab != null) { selectedTab.style.borderBottomWidth = 2f; }

        if (structureListController.FocusList(i)) {
            structureMenu.style.display = DisplayStyle.Flex;
            structureMenu.style.backgroundColor = color;

            selectedTab = button;
        
            SetTabBorderColours(selectedTab,Color.black);
            selectedTab.style.borderBottomWidth = 0f;

        } else {
            structureMenu.style.display = DisplayStyle.None;
        } 
        audioSource.PlayOneShot(tick,0.7f);
        
        //tutorial stuff:
        TutorialHighlighting();
    }

    private void factoryEnabledButtonPressed() {
        Debug.Log("factory enabled button pressed! current value=" + BlockManager.Instance.spawningEnabled);
        if (BlockManager.Instance.spawningEnabled){
            BlockManager.Instance.spawningEnabled = false;
            factoryEnabledButton.style.backgroundColor = new StyleColor( new Color32(110,37,38,255));
        } else {
            BlockManager.Instance.spawningEnabled = true;
            factoryEnabledButton.style.backgroundColor = new StyleColor( new Color32(55,155,55,255));
        }
    }


    private void TutorialHighlighting() {
        tutorialPrompt.text = "";
        TutorialHandler.StructurePlacementRequirements? req;
        if ((req = TutorialHandler.Instance.currentStep.structurePlacementRequirements) != null) {
            
            StructureSO targetStructure = req.Value.structure;

            Button targetTab = null;
            if (StructureHandler.Instance.IsCrystallizer(targetStructure)) targetTab = spawnersTabButton;
            if (StructureHandler.Instance.IsConveyor(targetStructure)) targetTab = conveyorsTabButton;
            if (StructureHandler.Instance.IsUpgrader(targetStructure)) targetTab = upgradersTabButton;
            if (StructureHandler.Instance.IsProcessor(targetStructure)) targetTab = collectorsTabButton;

            if (selectedTab != targetTab) {
                SetTabBorderColours(targetTab,Color.magenta);
                tutorialPrompt.text = $"Lets place a {targetStructure.structureName}. It can be find in the highlighted tab below";
            } 
            else if (BuildingSystem.Instance.GetToBuild() != targetStructure) {
                tutorialPrompt.text =  $"Click on the {targetStructure.structureName} to select it to be built";
            }
            else if (!BuildingSystem.Instance.PreviewPosition().Item1) {
                tutorialPrompt.text =  $"Move the structure preview on your mouse over to the pink indicator. \n You can press Q or E to rotate the structure preview left and right";
            }
            else {
                tutorialPrompt.text =  $"Click the Left mouse button to place the structure";
            }
        }

        if (TutorialHandler.Instance.currentStep.stepName == "Wait till cubits for infuser") {
            tutorialPrompt.text =  $"Now we are starting to make some cubits! \n Soon we'll have enough for an arcane infuser!";
        }

        if (TutorialHandler.Instance.currentStep.stepName == "Finish") {
            tutorialPrompt.text =  $"Now it's all up to you! Good luck setting up your dream gem factory!";
        }

        if (TutorialHandler.Instance.currentStep.stepName == "Enable Spawning") {
            tutorialPrompt.text =  $"Lets Enable Spawning so we can start making some Cubits! \n Click the enable factory button in the top left of the screen.";
            SetTabBorderColours(factoryEnabledButton,Color.magenta);
        } else {
            SetTabBorderColours(factoryEnabledButton,Color.black);
        }
        
    }

    private void SetTabBorderColours(Button tabButton, Color color) {
        tabButton.style.borderBottomColor = new StyleColor(color);
        tabButton.style.borderRightColor = new StyleColor(color);
        tabButton.style.borderLeftColor = new StyleColor(color);
        tabButton.style.borderTopColor = new StyleColor(color);
    }

    //Menu button methods
    void clearFactoryFloorButtonPressed() {
        BlockManager.Instance.ClearFactory();
    }
    private void Instant_UpdateTutorialFunctions(object sender, System.EventArgs e) {
        TutorialHighlighting();
    }
    //stuff idk
    private void Instant_ToBuildChanged(object sender, System.EventArgs e) {
        RefreshItemInfo();
        TutorialHighlighting();
    }
    private void Instant_TutorialStepCompleted(object sender, System.EventArgs e) {
        TutorialHighlighting();
    }

    private void RefreshItemInfo() {
        StructureSO toBuild;
        if ((toBuild = BuildingSystem.Instance.GetToBuild()) != null) {
            itemName.text = toBuild.structureName;
            itemDescription.text = toBuild.structureDescription;
            itemCost.text = $"Price: {toBuild.structureCost} Cubits";
        }
        else {
            itemName.text = "Nothing";
            itemDescription.text = "Select a structure below to begin building";
            itemCost.text = "";
        }
    }
    
    

    
    void Update() {
        //Screen.SetResolution(800, 600, false);

        currency.text = "cubits: $" + CurrencyManager.Instance.GetMoney();

        if (BlockManager.Instance.activeBlockCount < BlockManager.Instance.poolSize){
            progressBarLabel.text = BlockManager.Instance.activeBlockCount + " / " + BlockManager.Instance.poolSize;
            factoryFullnessBar.style.width = BlockManager.Instance.activeBlockCount * 1.1f; //bar width is 110, so...
            factoryHaltingLabel.visible = false;
        } else {
            progressBarLabel.text = "!!!" + BlockManager.Instance.activeBlockCount + " / " + BlockManager.Instance.poolSize + "!!!";
            factoryHaltingLabel.visible = true;
            
            BlockManager.Instance.spawningEnabled = false;
            factoryEnabledButton.style.backgroundColor = new StyleColor( new Color32(110,37,38,255));

        }
        

        structureListController.RefreshListIcons();

        //Moved input things from building system to UI
        if (Input.GetKeyDown(KeyCode.Alpha1)){
            conveyorsTabButtonPressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)){
            spawnersTabButtonPressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)){
            upgradersTabButtonPressed();
        }  
        if (Input.GetKeyDown(KeyCode.Alpha4)){
            collectorsTabButtonPressed();
        } 
        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        //gets top visualElement at mouseposition
        //root.GetFirstAncestorOfType;
    }
}
