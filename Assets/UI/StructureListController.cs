using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class StructureListController {
    
    VisualTreeAsset structureListEntryTemplate;
    ScrollView structureListScroll;

    VisualElement currentList;

    VisualElement[] structureLists;
    VisualElement conveyorList;
    VisualElement spawnerList;
    VisualElement upgraderList;
    VisualElement collectorList;

    public void SetupStructureList(VisualElement root, VisualTreeAsset listEntryTemplate) {
        structureListEntryTemplate = listEntryTemplate;

        structureListScroll = root.Q<ScrollView>("structureListScroll");

        structureLists = new VisualElement[4];

        structureLists[0] = SetupList(StructureHandler.Instance.conveyors);
        structureLists[1] = SetupList(StructureHandler.Instance.crystallizers);
        structureLists[2] = SetupList(StructureHandler.Instance.infusers);
        structureLists[3] = SetupList(StructureHandler.Instance.processors);
    }

    public void RefreshListIcons() {
        if (currentList == null) return;
        foreach (VisualElement visualElement in currentList.Children()) {
            ((StructureListEntryController)visualElement.userData).RefreshInfo();
        }
    }

    public bool FocusList(int listIndex) {
        if (currentList != null) {
            structureListScroll.Remove(currentList);
        }
        if (currentList != structureLists[listIndex]) {
            currentList = structureLists[listIndex];
            structureListScroll.Add(structureLists[listIndex]);
            return true;
        } else {
            currentList = null;
            return false;
        }
    }

    private VisualElement SetupList(List<StructureSO> structureData){
        VisualElement listElement = new VisualElement {
            style = { flexDirection = FlexDirection.Row }
        };
        
        structureData.ForEach(structureSO => {
            VisualElement structureListEntry = structureListEntryTemplate.Instantiate();
            StructureListEntryController structureListEntryController = new StructureListEntryController();
            structureListEntry.userData = structureListEntryController;
            structureListEntryController.SetupListEntry(structureListEntry,structureSO);

            listElement.Add(structureListEntry);
        });

        return listElement;
    }
}
