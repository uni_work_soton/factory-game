using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class StructureListEntryController {
    private Button structureButton;
    private VisualElement structureIcons;
    private Label structureStored;
    private StructureSO structureSO;
    private VisualElement[] stored;
    private bool _isStructureIconNotNull;
    private StyleBackground structureSprite;

    public void SetupListEntry(VisualElement visualElement, StructureSO structureSO) {
        structureButton = visualElement.Q<Button>("structureButton");
        structureIcons = visualElement.Q<VisualElement>("structureIcons");
        structureStored = visualElement.Q<Label>("structureStored");
        this.structureSO = structureSO;
        stored = new VisualElement[24];

        structureButton.clicked += () => {
            BuildingSystem.Instance.SetToBuild(structureSO);
        };

        _isStructureIconNotNull = structureSO.structureIcon != null;
        if (_isStructureIconNotNull) {
            structureSprite = new StyleBackground(structureSO.structureIcon.texture);
        }
        SetGraphic();
    }

    private void SetGraphic() {
        if (_isStructureIconNotNull) {
            structureButton.text = String.Empty;
            for (int i = 23; i >= 0; i--) {
                //create all the sprites in the correct places up until its full, then just activate and deactivate in refresh
                int row = (int)(-0.5f + Mathf.Sqrt(0.25f + 2 * i));
                int col = (int)(i - (row * (row + 1)) / 2.0f);
                int adjustedCol = (row + 1) / 2 + (-2 * (col % 2) + 1) * (col + 1) / 2;
                
                // (col+1) / 2 +  
                
                int yOffset = row * -8;
                int xOffset = (adjustedCol * 32) - (row * 16); //(int) (i - (row * (row + 1)) / 2.0f);
                //Debug.Log($"placing sprite at {row}:{xOffset}");
                // multiply the col by the sprite width (32), then take away the row amount * half the sprite width, 


                VisualElement sprite = new VisualElement {
                    style = {
                        display = DisplayStyle.Flex,
                        width = new StyleLength(new Length(32f, LengthUnit.Pixel)),
                        height = new StyleLength(new Length(32f, LengthUnit.Pixel)),
                        backgroundImage = new StyleBackground(structureSO.structureIcon.texture),
                        position = new StyleEnum<Position>(Position.Absolute),
                        top = 11 + yOffset,
                        left = 7 + xOffset,
                        right = 8 - yOffset,
                        bottom = 4 - xOffset
                    }
                };

                stored[i] = sprite;
                structureIcons.Add(sprite);
            }
        }
        else {
            structureButton.text = structureSO.structureName;
        }
    }
    
    public void RefreshInfo() {
        TutorialHandler.StructurePlacementRequirements? req;
        if (BuildingSystem.Instance.GetToBuild() == structureSO) {
            SetBorderColor(Color.white);
        }
        else if ((req = TutorialHandler.Instance.currentStep.structurePlacementRequirements) != null) {
            SetBorderColor(req.Value.structure == structureSO ? Color.magenta : Color.black);
        }
        else {
            SetBorderColor(Color.black);
        }
        
        
        int count = StructureHandler.Instance.GetStorageCount(structureSO);
        if (_isStructureIconNotNull) {
            for (int i = 1; i < 24; i++) {
                stored[i].style.display = (i < count) ? DisplayStyle.Flex: DisplayStyle.None;
            }
        }
        if (count > 0) {
            structureButton.style.backgroundColor = new StyleColor(new Color32(75,128,202,255));
            structureStored.text = count.ToString();
            return;
        }

        structureStored.text = "";
        if (CurrencyManager.Instance.GetMoney() >= structureSO.structureCost) {
            structureButton.style.backgroundColor = new StyleColor(new Color32(138,176,96,255));
            return;
        }
        structureButton.style.backgroundColor = new StyleColor(new Color32(180,82,82,255));
    }
    
    
    private void SetBorderColor(Color color) {
        structureButton.style.borderBottomColor = new StyleColor(color);
        structureButton.style.borderRightColor = new StyleColor(color);
        structureButton.style.borderLeftColor = new StyleColor(color);
        structureButton.style.borderTopColor = new StyleColor(color);
    }
}
